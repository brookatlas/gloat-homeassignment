import click
from requests.exceptions import HTTPError
import requests



class CandidateMatcherRequests:
    def __init__(self, base_uri:str = "http://localhost:8000"):
        self.base_uri = base_uri

    @property
    def api_uri(self):
        return f'{self.base_uri}/api'

    def get_jobs(self):
        try:
            uri = f'{self.api_uri}/jobs'
            response = requests.get(uri)
            response.raise_for_status()
            jobs = response.json().get('jobs')
            for job in jobs:
                print(f"title: {job.get('title')}, skill: {job.get('skill')}, id: {job.get('id')}")
        except HTTPError as e:
            print('could not retrieve jobs properly')

    def get_job(self, id:int):
        try:
            uri = f'{self.api_uri}/jobs/{id}'
            response = requests.get(uri)
            response.raise_for_status()
            job = response.json()
            print(f"title: {job.get('title')}, skill: {job.get('skill')}, id: {job.get('id')}")
        except HTTPError as e:
            if e.response.status_code == 404:
                print(f'the job with id {id} was not found')
            else:
                print('could not retrieve the job properly')
    
    def create_job(self, title:str, skill:str):
        try:
            body = {
                'title': title,
                'skill': skill
            }
            uri = f'{self.api_uri}/jobs/'
            response = requests.post(uri, json=body)
            response.raise_for_status()
            job_created = response.json()
            print('job created successfully \n')
            print ('job data: \n')
            print(f'job title: {job_created.get("title")}, job skill: {job_created.get("skill")}, id: {job_created.get("id")}')
        except HTTPError as e:
            if e.response.status_code == 409:
                print('job already exists')
            else:
                print('could not create job properly. is the django server on?')
        
    def match_job(self, id:int):
        try:
            uri = f'{self.api_uri}/jobs/{id}/match'
            response = requests.get(uri)
            response.raise_for_status()
            matches = response.json().get('matches')
            for match in matches:
                print(f"title: {match.get('title')}, skills: {match.get('skills')}")
        except HTTPError as e:
            if e.response.status_code == 404:
                print(f'the job with id {id} was not found')
            else:
                print('could not retrieve the candidate matches')
    
    def delete_job(self, id:int):
        try:
            uri = f'{self.api_uri}/jobs/{id}'
            response = requests.delete(uri)
            response.raise_for_status()
            print(f'the job with id {id} was deleted successfully.')    
        except HTTPError as e:
            if e.response.status_code == 404:
                print(f'the job with id {id} was not found')
            else:
                print('could not delete the given job')
    
    def get_candidates(self):
        try:
            uri = f'{self.api_uri}/candidates'
            response = requests.get(uri)
            response.raise_for_status()
            candidates = response.json().get('candidates')
            if len(candidates) == 0:
                print ("there are no candidates. please create some.")
            else:
                print('candidates retrieved')
            for candidate in candidates:
                print(f'title: {candidate.get("title")}, skills: {candidate.get("skills")}, id: {candidate.get("id")}')
        except HTTPError as e:
            print("could not retrieve candidates properly")
    
    def get_candidate(self, id):
        try:
            uri = f'{self.api_uri}/candidates/{id}'
            response = requests.get(uri)
            response.raise_for_status()
            candidate = response.json()
            print(f'title: {candidate.get("title")}, skills: {candidate.get("skills")}, id: {candidate.get("id")}')
        except HTTPError as e:
            if e.response.status_code == 404:
                print('candidate with the given id does not exist')
            else:
                print('could not retrieve candidate properly')
        
    def create_candidate(self, title:str, skills:list[str]):
        try:
            uri = f'{self.api_uri}/candidates/'
            body = {
                'title': title,
                'skills': skills
            }
            response = requests.post(uri, json=body)
            response.raise_for_status()
            candidate = response.json()
            print(f'title: {candidate.get("title")}, skills: {candidate.get("skills")}, id: {candidate.get("id")}')
        except HTTPError as e:
            if e.response.status_code == 409:
                print('candidate with such details already exists')
            else:
                print('could not create the candidate properly. is the django server running?')


    def update_candidate(self, id:int, title:str = None, skills:list[str] = None):
        try:
            body = {}
            if title:
                body['title'] = title
            if skills:
                body['skills'] = skills
            uri = f'{self.api_uri}/candidates/{id}'
            response = requests.patch(uri, json=body)
            response.raise_for_status()
            candidate = response.json()
            print(f'title: {candidate.get("title")}, skills: {candidate.get("skills")}, id: {candidate.get("id")}')
        except HTTPError as e:
            if e.response.status_code == 404:
                print('job with given id does not exist.')
            else:
                print('could not update the candidate properly. is the django server running?')
        
    def delete_candidate(self, id):
        try:
            uri = f'{self.api_uri}/candidates/{id}'
            response = requests.delete(uri)
            response.raise_for_status()
            print(f'candidate with id {id} was deleted successfully.')
        except HTTPError as e:
            if e.response.status_code == 404:
                print('the candidate with the given id does not exist.')
            else:
                print('the candidate with the given id could not be deleted. is the django server running?')
        


    

global CandidateMatcherHelper
CandidateMatcherHelper = CandidateMatcherRequests("http://localhost:8000")



@click.group()
def cli():
    pass

@click.group()
def jobs():
    pass

@click.group()
def candidates():
    pass

@click.command(name='get')
@click.option('--id', default=None)
def get_jobs(id):
        if id:
            CandidateMatcherHelper.get_job(id)
        else:
            CandidateMatcherHelper.get_jobs()


@click.command(name='match')
@click.argument("id", required=True)
def match_job(id):
    CandidateMatcherHelper.match_job(id)

@click.command(name='create')
def create_job():
    title = input('title of job to create? ')
    skill = input('skill required for the job?')
    CandidateMatcherHelper.create_job(title, skill)

@click.command(name='delete')
def delete_job():
    id = input('id of job to delete?')
    CandidateMatcherHelper.delete_job(id)


@click.command(name='get')
@click.option("--id", default=None)
def get_candidates(id):
    if id:
        CandidateMatcherHelper.get_candidate(id)
    else:
        CandidateMatcherHelper.get_candidates()

@click.command(name='create')
def create_candidate():
    title = input('title of candidate to create? ')
    skills = []
    skill = input('skills required for the candidate? (enter -1 when you are done)')
    while skill != "-1":
        skills.append(skill)
        skill = input('skills required for the candidate? (enter -1 when you are done)')
    CandidateMatcherHelper.create_candidate(title, skills)

@click.command(name='update')
def update_candidate():
    id = input('id of candidate to update?')
    update_title = input('update the title? y/n')
    while update_title not in ['y', 'n']:
        update_title = input('wrong input. update the title? y/n')
    update_skills = input('update the skills? y/n')
    while update_skills not in ['y', 'n']:
        update_skills = input('wrong input. update the skills? y/n')
    updated_title = None
    updated_skills = []
    if update_title == 'y':
        updated_title =  input('pick a new title')
    if update_skills == 'y':
        updated_skill = input('pick new skills? (enter -1 when your\'e done)')
        while updated_skill != "-1":
            updated_skills.append(updated_skill)
            updated_skill = input('pick new skills? (enter -1 when your\'e done)')
    CandidateMatcherHelper.update_candidate(id, updated_title, updated_skills)
    

@click.command(name='delete')
def delete_candidate():
    id = input("candidate's id to delete?")
    CandidateMatcherHelper.delete_candidate(id)


jobs.add_command(get_jobs)
jobs.add_command(match_job)
jobs.add_command(create_job)
jobs.add_command(delete_job)
candidates.add_command(get_candidates)
candidates.add_command(create_candidate)
candidates.add_command(update_candidate)
candidates.add_command(delete_candidate)
cli.add_command(candidates)
cli.add_command(jobs)


@click.group()
def jobs():
    pass

if __name__ == '__main__':
    cli()