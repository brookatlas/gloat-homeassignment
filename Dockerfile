FROM python:alpine3.12
RUN mkdir -p /opt/app
COPY . /opt/app
WORKDIR /opt/app
RUN python -m ensurepip
RUN pip install -r requirements.txt
RUN python3 manage.py migrate
CMD ["gunicorn", "matcher.wsgi", "--bind", "0.0.0.0:8000"]