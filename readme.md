## candidate-matcher app
a basic app that can match candidates to a job given.
the app is written with django 3.2

since the app is for demostration porpuses, it uses a local sqlite DB file.

### installation requirements
* have python 3.9.4 or newer installed
* any operating system such as windows or linux would work  
* for windows: have git bash installed and enabled in your terminal/ide  
* for linux: assuming you have all dependencies installed for building django projects(python, etc)  


### deployment instructions
#### deploy locally on linux and windows:
* note, if using windows, make sure you are using the git bash terminal/any other terminal supporting git and virtual environments, in your ide of choice(a.k.a vs code, pycharm)  
steps:  
clone the repository:  
`git clone https://gitlab.com/brookatlas/gloat-homeassignment.git`  
get into the repository folder:  
`cd gloat-homeassignment`  
create a virtual environment for the project  
`python3.8 -m venv .venv`  
install the project's requirements   
`pip install -r requirements.txt`  
build the local database:  
`python manage.py migrate`  
run the django based server(we may use any port we like)  
`python manage.py runserver 0.0.0.0:8000`  
we may now access the API via our localhost  
`localhost:8000/api/`


#### deployment with docker(recommended)
build the image:  
`docker build ./ --tag candidate-finder:0.1`  
run the image:  
`docker run -d -p 8000:8000 candidate-finder`  
we may now access the API via our localhost    
`http://localhost:8000/api/`


#### running the unit tests    
you may run the unit tests with the following command( in venv):  
`python manage.py test`  

#### matcherCli
in order to edit the entities in the app, we may use the 'matcherCli.py' cli app(written with click)
we must make sure the django app is running in the background, to perform the backend-side for us of course.

command docs:  
        candidates:  
            `python matcherCli.py candidates get` - retrieves all candidates  
            `python matcherCli.py candidates get --id 1` - retrieves a candidate by id of 1  
            `python matcherCli.py candidates create` - creates a new candidate  
            `python matcherCli.py candidates create` - updates an existing candidate(can be partial fields only as well)  
            `python matcherCli.py candidates delete` - deletes an existing candidate by id  
        jobs:  
            `python matcherCli.py jobs get` - retrieves all jobs  
            `python matcherCli.py jobs get --id 1` - retrives a job by id of 1  
            `python matcherCli.py job create` - creates a new job  
            `python matcherCli.py job delete` - deletes an existing job by id  



### api documentation  
base_url: <HOST_RUNNING_ON>:8000/api  
#### resources  

#### candidates   
get all candidates:  
    url: <HOST_RUNNING_ON>:8000/api/candidates  
    method: GET  
get specific candidate by id:  
    url: <HOST_RUNNING_ON>:8000/api/candidates/<CANDIDATE_ID>  
    method: GET  
create candidate:    
    url: <HOST_RUNNING_ON>:8000/api/candidates/  
    method: POST  
    body(json):  
    ;;;  
    {  
        "title": "example candidate",  
        "skills": [  
            "java", "python", "C++"  
        ]  
    }  
    ;;;  
update candidate:  
    url: <HOST_RUNNING_ON>:8000/api/candidates/<CANDIDATE_TO_UPDATE_ID>  
    method: PATCH  
    all fields may be given or not(update can be partial)  
    body(json):   
    ;;;  
    {  
        "title": "example candidate", (optional)  
    }  
    ;;;  
delete candidate:  
    url: <HOST_RUNNING_ON>:8000/api/candidates/<CANDIDATE_TO_DELETE_ID>  
    method: DELETE  
  
#### jobs  
get all jobs:  
    url: <HOST_RUNNING_ON>:8000/api/jobs  
    method: GET  
get specific job by id:   
    url: <HOST_RUNNING_ON>:8000/api/jobs/<JOB_ID>  
    method: GET    
create job:    
    url: <HOST_RUNNING_ON>:8000/api/jobs/   
    method: POST    
    body(json):    
    ;;;  
    {  
        "title": "software engineer",  
        "skill": "golang"  
    }  
    ;;;  
delete job:  
    url: <HOST_RUNNING_ON>:8000/api/candidates/<JOB_ID_TO_DELETE>  
    method: DELETE  


### notes and considerations for this implementation(for assignment checker)  
* problems I had to tackle:  
  
* the biggest problem I was debating on...was I supposed to make a rest api or a regular django app with forms?(was not realy told in the assignment doc)  
* in the end, with a big tackle to go, I wrote some custom decorators that helped me to validate requests with json schema's and make sure the body is there  
* that's it for the technological tackle  
  
  
* things you may notice:  
  
* some url's may require a slash which is missing in the docs(typo) (django setting)  
* debug is set to true(even on gunicorn, I know this is not a recommended thing to do in production)  
   
   
* technological debate I had on my mind?:
  
    
* I had a debate on weather to use layers of service to access data(since Django is tightly integrated, its a big known debate)  
* yet, I decided to do divide the app to models(data access), services(functionallity access), and to routes(views)  
  
  
* things I made sure to do:
  
* made sure to write code clean and neat(use DI, IOC)  
* used singletons to keep memory waste away  
* wrote unit tests
* the matcher cli kinda counts as a integration test ;)(written in rush compared to the whole app, but works OK)
  
  
* things I did not do fully but could have done:
* add gitlab CI(could have been quite easily written)  
* add more test for better coverage( I know I could have done better, but time was limited, I guess for an exrecise it is good enough)  


