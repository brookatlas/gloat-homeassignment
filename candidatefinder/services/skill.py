from django.forms import model_to_dict
from django.core.exceptions import ObjectDoesNotExist
from candidatefinder.utils.decorators import singleton
from candidatefinder.utils.exceptions import SkillAlreadyExists, SkillNotFound
from candidatefinder.models.skill import Skill


@singleton
class SkillService():
    def create(self, name:str) -> Skill:
        try:
            Skill.objects.get(name=name)
            raise SkillAlreadyExists(name=name)
        except ObjectDoesNotExist:
            skill = Skill(name)
            skill.save()
            return model_to_dict(skill)

    def read(self, name:str = None) -> list[Skill]:
        if name:
            return self.__read_by_name(name)
        else:
            return self.__read_all()

    def read_by_names(self, skill_names:list[str]) -> list[Skill]:
        try:
            skills = Skill.objects.filter(name__in=skill_names)
            return [model_to_dict(skill) for skill in skills]
        except ObjectDoesNotExist:
            raise SkillNotFound(skill_names)

    def __read_by_name(self, name:str) -> Skill:
        try:
            result = Skill.objects.get(name=name)
            return model_to_dict(result)
        except ObjectDoesNotExist:
            raise SkillNotFound(name=name)

    def __read_all(self) -> list[Skill]:
        all_skills = Skill.objects.all()
        return [model_to_dict(skill) for skill in all_skills]

    def delete(self, name:str) -> bool:
        try:
            skill_obj = Skill.objects.get(name=name)
            skill_obj.delete()
            return True
        except ObjectDoesNotExist:
            raise SkillNotFound(name)
