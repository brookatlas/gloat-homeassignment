from django.forms import model_to_dict
from django.core.exceptions import ObjectDoesNotExist
from candidatefinder.utils.decorators import singleton
from candidatefinder.models.job import Job
from candidatefinder.models.skill import Skill
from candidatefinder.utils.exceptions import JobNotFound, \
     JobAlreadyExists, SkillNotFound, JobByIdNotFound, \
     CandidatesNotFound, NoJobMatchesFound
from candidatefinder.services.candidate import CandidateService
from candidatefinder.models.candidate import Candidate
from candidatefinder.services.skill import SkillService


@singleton
class JobService():
    def __init__(self, 
                 candidate_service:CandidateService = CandidateService()):
        self.candidate_service = candidate_service

    def create(self, job_dict:dict) -> Job:
        job_obj = self.__create_job_if_not_exists(job_dict)
        skill_obj = self.__create_skill_if_not_exists(job_dict.get('skill'))
        self.__add_skill_to_job(job_obj, skill_obj)
        job_obj.save()
        return self.__serialize_job(job_obj)
        
    def __create_job_if_not_exists(self, job_dict:dict) -> Job:
        try:
            Job.objects.get(
                title=job_dict.get('title'), 
                skill__name=job_dict.get('skill')
            )
            raise JobAlreadyExists(
                job_dict.get('title'), 
                job_dict.get('skill')
            )
        except ObjectDoesNotExist as e:
            job_obj = Job(
                title=job_dict.get('title')
            )
            return job_obj
        
    def __create_skill_if_not_exists(self, skill_name:str) -> Skill:
        try:
            skill = SkillService().read(skill_name)
            return Skill.objects.get(name=skill_name)
        except SkillNotFound as e:
            skill = SkillService().create(skill_name)
        finally:
            return Skill.objects.get(name=skill_name)

    def __add_skill_to_job(self, job_obj:Job, skill_obj:Skill):
        job_obj.skill = skill_obj

    def read(self) -> list[dict]:
        return self.__serialize_jobs(Job.objects.all())

    def read_specific(self, title:str, skill:str) -> dict:
        try:
            result = Job.objects.get(
                title=job_dict.get('title'), 
                skill__name=job_dict.get('skill')
            )
            result = self.__serialize_job(result)
        except ObjectDoesNotExist as e:
            raise JobNotFound(title, skill)
        finally:
            return result
    
    def read_by_id(self, id:int) -> dict:
        try:
            result = Job.objects.get(pk=id)
            result = self.__serialize_job(result)
            return result
        except ObjectDoesNotExist as e:
            raise JobByIdNotFound(id)

    def __serialize_job(self, job:Job):
        serialized_obj = model_to_dict(job)
        serialized_obj['id'] = job.pk
        serialized_obj['skill'] = model_to_dict(job.skill)
        return serialized_obj

    def __serialize_jobs(self, jobs:list[Job]):
        return [self.__serialize_job(job) for job in jobs]
    
    def delete(self, job_id:int) -> bool:
        try:
            job_obj = Job.objects.get(pk=job_id)
            job_obj.delete()
            return True
        except ObjectDoesNotExist as e:
            raise JobByIdNotFound(job_id)
    
    def match(self, job_id:int) -> list[dict]:
        try:
            job_obj = Job.objects.get(pk=job_id)
            matching_candidates = self.candidate_service.read_by_properties(
                job_obj.title, 
                job_obj.skill
            )
            return matching_candidates
        except CandidatesNotFound as e:
            return []
        except ObjectDoesNotExist as e:
            raise JobByIdNotFound(job_id)
