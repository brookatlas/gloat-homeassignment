from django.forms import model_to_dict
from candidatefinder.utils.decorators import singleton
from candidatefinder.utils.exceptions import CandidateAlreadyExists, \
     CandidateNotFound, SkillNotFound
from candidatefinder.models.candidate import Candidate
from candidatefinder.models.skill import Skill
from candidatefinder.services.skill import SkillService
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count
from typing import List


@singleton
class CandidateService():
    def __init__(self, skill_service:SkillService = SkillService()): 
        self.skill_service = skill_service

    def create(self, candidate_dict: dict) -> Candidate:
        try:
            matching_skills = self.skill_service.read_by_names(
                candidate_dict.get('skills')
            )
            matching_results = Candidate.objects.get(
                skills__name__in = candidate_dict.get('skills'),
                title=candidate_dict.get('title')
            )
            if len(matching_results.skills) == len(candidate_dict.get('skills')):
                raise CandidateAlreadyExists(
                    candidate_dict.get('title'), 
                    candidate_dict.get('skills')
                )
            else:
                raise ObjectDoesNotExist
        except ObjectDoesNotExist:
            skills_created = self.__create_candidates_skills(candidate_dict)
            candidate_obj = Candidate(
                title=candidate_dict.get('title')
            )
            candidate_obj.save()
            self.__add_skills_to_candidate(candidate_obj, skills_created)
            candidate_obj.save()
            return self.__read_candidate(candidate_obj)

    def __get_candidate_matching_skills(self, candidate_dict:dict):

        matching_skills = []
        for skill in candidate_dict.get('skills'):
                matching_skills.append(Skill.objects.get(name=skill))
        return matching_skills

    def __create_candidates_skills(self, candidate_dict:dict):
        skill_list = []
        skills = candidate_dict.get('skills')
        for skill in skills:
            skill = self.__create_candidate_skill(skill)
            skill_list.append(skill)
        return skill_list

    def __create_candidate_skill(self, name:str):
        try:
            result = self.skill_service.read(name=name)
            return Skill.objects.get(name=name)
        except SkillNotFound:
            skill_created = Skill(name=name)
            skill_created.save()
            return skill_created

    def __add_skills_to_candidate(self, candidate:Candidate, 
                                  skill_list:list[Skill]):
        for skill in skill_list:
            candidate.skills.add(skill)

    def read(self, id:int = None) -> list[Candidate]:
        if id:
            return self.__read_by_id(id)
        else:
            return self.__read_all()

    def read_by_properties(self, title:str, skill:str): 
        try:
            candidates = Candidate.objects.filter(
                title=title, 
                skills__in=[skill]
            )
            return self.__read_candidates(candidates)
        except ObjectDoesNotExist:
            raise CandidatesNotFound(title, skill)

    def __read_by_id(self, id:int) -> List[Candidate]: 
        try:
            result = Candidate.objects.get(pk=id)
            return self.__read_candidate(result)
        except ObjectDoesNotExist:
            raise CandidateNotFound(id)

    def __read_all(self) -> List[Candidate]: 
        candidates = Candidate.objects.all()
        return self.__read_candidates(candidates)

    def __read_candidate(self, candidate:Candidate) -> dict: 
        result = model_to_dict(candidate)
        result['id'] = candidate.pk
        result['skills'] = self.__serialize_candidate_skills(result.get('skills'))
        return result

    def __serialize_candidate_skills(self, skills:list[Skill]) -> dict:
        skills_list = []
        for skill in skills:
            skills_list.append(model_to_dict(skill))
        return skills_list

    def __read_candidates(self, candidateList:List[Candidate]) -> dict:
        candidates = []
        for candidate in candidateList:
            candidates.append(self.__read_candidate(candidate))
        return candidates

    def __remove_all_skills_from_candidate(self, candidate:Candidate): 
        for skill in candidate.skills.all(): 
            candidate.skills.remove(skill)

    def update(self, candidate_id:int, new_candidate_dict:dict) -> Candidate:
        try:
            candidate = Candidate.objects.get(pk=candidate_id)
            if new_candidate_dict.get('title'):
                candidate.title = new_candidate_dict.get('title')
            if isinstance(new_candidate_dict.get('skills'), list):
                skills = self.__create_candidates_skills(new_candidate_dict)
                self.__remove_all_skills_from_candidate(candidate)
                self.__add_skills_to_candidate(candidate, skills)
            candidate.save()
            return self.__read_candidate(candidate)
        except ObjectDoesNotExist:
            raise CandidateNotFound(candidate_id)

    def delete(self, candidate_id:int) -> bool:
        try:
            candidate_obj = Candidate.objects.get(pk=candidate_id)
            candidate_obj.delete()
        except ObjectDoesNotExist:
            raise CandidateNotFound(candidate_id)
