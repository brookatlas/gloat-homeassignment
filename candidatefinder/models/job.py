from django.db import models
from candidatefinder.models.skill import Skill


class Job(models.Model):
    '''
        model name: job
        description: represents a job.
        fields:
            * title - the title of the job/position
            * skill - a single skill that represents a requirement for this job/position.
    '''
    title = models.CharField(max_length=100)
    skill = models.ForeignKey(to=Skill, related_name='skill', on_delete=models.PROTECT)
    class Meta:
        app_label = 'candidatefinder'


global JobSchema
JobSchema = {
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "title": "brook",
            "skill": "asdasd"
        }
    ],
    "required": [
        "title",
        "skill"
    ],
    "properties": {
        "title": {
            "$id": "#/properties/title",
            "type": "string",
            "title": "The title schema",
            "minLength": 1,
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "brook"
            ]
        },
        "skill": {
            "$id": "#/properties/skill",
            "type": "string",
            "minLength": 1,
            "title": "The skill schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "asdasd"
            ]
        }
    },
    "additionalProperties": True
}
