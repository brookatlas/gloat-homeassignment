from django.db import models


class Skill(models.Model):
    '''
        model name: Skill
        description: 
    '''
    name = models.CharField(max_length=100, primary_key=True)

    class Meta:
        app_label = 'candidatefinder'
