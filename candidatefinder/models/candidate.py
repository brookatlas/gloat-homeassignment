from django.db import models
from candidatefinder.models.skill import Skill


class Candidate(models.Model):
    '''
        model name: candidate
        description: represents a job candidate. 
        fields:
        title - title of candidate
        skills - set of candidate's skills
    '''
    title = models.CharField(max_length=100)
    skills = models.ManyToManyField(to=Skill)


    class Meta:
        app_label = 'candidatefinder'

'''
    json schemas section in here.
'''


'''
    name: candidateSchema
    purpose: for validating the creation(POST) of new candidates
'''
global CandidateSchema
CandidateSchema = {
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "title": "brook",
            "skills": []
        }
    ],
    "required": [
        "title",
        "skills"
    ],
    "properties": {
        "title": {
            "$id": "#/properties/title",
            "type": "string",
            "minLength": 1,
            "title": "The title schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "brook"
            ]
        },
        "skills": {
            "$id": "#/properties/skills",
            "type": "array",
            "title": "The skills schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "examples": [
                []
            ],
            "additionalItems": True,
            "items": {
                "$id": "#/properties/skills/items"
            }
        }
    },
    "additionalProperties": True
}


'''
    name: UpdateCandidateSchema
    purpose: meant for PATCH requests(partial update)
'''
global UpdateCandidateSchema
UpdateCandidateSchema = {
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "title": "brook",
            "skills": [
                "asdasd"
            ]
        }
    ],
    "required": [
    ],
    "properties": {
        "title": {
            "$id": "#/properties/title",
            "type": "string",
            "title": "The title schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "brook"
            ]
        },
        "skills": {
            "$id": "#/properties/skills",
            "type": "array",
            "title": "The skills schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "examples": [
                [
                    "asdasd"
                ]
            ],
            "additionalItems": True,
            "items": {
                "$id": "#/properties/skills/items",
                "anyOf": [
                    {
                        "$id": "#/properties/skills/items/anyOf/0",
                        "type": "string",
                        "title": "The first anyOf schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "asdasd"
                        ]
                    }
                ]
            }
        }
    },
    "additionalProperties": True
}
