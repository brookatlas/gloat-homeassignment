
from django.urls import path
from candidatefinder.routes.candidate import CandidateView, CandidateDetailView
from candidatefinder.routes.job import JobView, DetailedJobView, MatchJobView

urlpatterns = [
    path('candidates/', CandidateView.as_view()),
    path('candidates/<int:id>', CandidateDetailView.as_view()),
    path('jobs/', JobView.as_view()),
    path('jobs/<int:id>', DetailedJobView.as_view()),
    path('jobs/<int:id>/match', MatchJobView.as_view())
]
