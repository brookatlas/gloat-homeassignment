from candidatefinder.services.candidate import CandidateService
from candidatefinder.services.skill import SkillService
from candidatefinder.services.job import JobService
from candidatefinder.models.candidate import Candidate
from candidatefinder.models.job import Job
from candidatefinder.models.skill import Skill
from django.test import TestCase


# Create your tests here.
class SkillServiceTestCase(TestCase):
    def setUp(self):
        skill_1 = Skill(name='test_skill')
        skill_1.save()
        skill_2 = Skill(name='test_skill2')
        skill_2.save()

    def test_read_skills(self):
        skill_service = SkillService()
        skills = skill_service.read()
        assert len(skills) == 2
        skill_names = [skill.get('name') for skill in skills]
        assert skill_names == ['test_skill', 'test_skill2']

    def test_create_skill_service(self):
        skill_service = SkillService()
        skill_service.create('test_skill3')
        skills = skill_service.read()
        assert len(skills) == 3
        skill_names = [skill.get('name') for skill in skills]
        assert skill_names == ['test_skill', 'test_skill2', 'test_skill3']

    def test_delete_skill_service(self):
        skill_service = SkillService()
        skills = skill_service.read()
        assert len(skills) > 0
        for skill in skills:
            skill_service.delete(skill.get('name'))
        skills = skill_service.read()
        assert len(skills) == 0


class JobServiceTestCase(TestCase):
    def setUp(self):
        skill_1 = Skill(name='test_skill')
        skill_1.save()
        skill_2 = Skill(name='test_skill2')
        skill_2.save()
        job_1 = Job(
            skill=skill_1,
            title='test skill engineer'
        )
        job_1.save()
        job_2 = Job(
            skill=skill_2,
            title='test two skill engineer'
        )
        job_2.save()

    def test_read_jobs(self):
        job_service = JobService()
        skill_service = SkillService()
        skills = skill_service.read()
        jobs = job_service.read()
        for job in jobs:
            assert job.get('title') in ['test skill engineer', 'test two skill engineer']
            assert job.get('skill').get('name') in [skill.get('name') for skill in skills]
        assert len(jobs) == 2
    
    def test_create_jobs(self):
        job_service = JobService()
        jobs = job_service.read()
        assert len(jobs) == 2
        job_service.create({
            'title': 'my test job',
            'skill': 'my skill'
        })
        jobs = job_service.read()
        assert len(jobs) == 3
        job_created = Job.objects.get(title='my test job')
        assert job_created is not None


    def test_delete_jobs(self):
        job_service = JobService()
        jobs = job_service.read()
        assert len(jobs) == 2
        for job in Job.objects.all():
            job_service.delete(job.pk)
        jobs = job_service.read()
        assert len(jobs) == 0


class CandidateServiceTestCase(TestCase):
    def setUp(self):
        skill_1 = Skill(name='test_skill')
        skill_1.save()
        skill_2 = Skill(name='test_skill2')
        skill_2.save()
        job_1 = Job(
            skill=skill_1,
            title='test skill engineer'
        )
        job_1.save()
        job_2 = Job(
            skill=skill_2,
            title='test two skill engineer'
        )
        job_2.save()
        candidate_1 = Candidate(
            title='my candidate 1',
        )
        candidate_1.save()
        candidate_1.skills.add(skill_1)
        candidate_1.save()
        candidate_2 = Candidate(
            title='my candidate 2'
        )
        candidate_2.save()
        candidate_2.skills.add(skill_2)
        candidate_2.save()
        candidate_3 = Candidate(
            title='my candidate 3',
        )
        candidate_3.save()

    def test_read_candidates(self):
        candidate_service = CandidateService()
        candidates = candidate_service.read()
        assert len(candidates) == 3
        for candidate in candidates:
            assert candidate.get('title') in ['my candidate 1', 'my candidate 2', 'my candidate 3']
        candidate = candidate_service.read_by_properties('my candidate 2', Skill.objects.get(name='test_skill2'))
        assert candidate is not None

    def test_create_candidates(self):
        candidate_service = CandidateService()
        candidates = candidate_service.read()
        assert len(candidates) == 3
        candidate_created = candidate_service.create({
            'title': 'brookatlas',
            'skills': [
                'JAVA',
                'C++'
            ]
        })
        candidates = candidate_service.read()
        assert len(candidates) == 4
        assert len(Candidate.objects.all()) == 4
        

    def test_update_candidates(self):
        candidate_service = CandidateService()
        candidates = candidate_service.read()
        assert len(candidates) == 3
        candidate_to_update = Candidate.objects.get(title='my candidate 3')
        candidate_service.update(
            candidate_to_update.pk, 
            {
                'skills': []
            }
        )
        updated_candidate = Candidate.objects.get(title='my candidate 3')
        assert len(updated_candidate.skills.all()) == 0

    def test_delete_candidates(self):
        candidate_service = CandidateService()
        candidates = candidate_service.read()
        assert len(candidates) == 3
        candidate_pk_list = [c.pk for c in Candidate.objects.all()]
        for pk in candidate_pk_list:
            candidate_service.delete(pk)
        candidates = candidate_service.read()
        assert len(candidates) == 0
        assert len(Candidate.objects.all()) == 0
        