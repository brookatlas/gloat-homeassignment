# skill related exceptions
class SkillNotFound(Exception):
    def __init__(self, name):
        self.name = name
        super().__init__(self, self.name)

    def __str__(self):
        if isinstance(self.name, str):
            exp_str = f'skill named: {self.name} was not found'
        elif isinstance(self.name, list):
            exp_str = \
                + f'skills with the following names were not found: {self.name}'
        return exp_str


class SkillAlreadyExists(Exception):
    def __init__(self, name):
        self.name = name
        super().__init__(self, self.name)

    def __str__(self):
        exp_str = f'skill named: {self.name} already exists'
        return exp_str


# job related exceptions 
class JobNotFound(Exception):
    def __init__(self, title, skill):
        self.title = title
        self.skill = skill
        super().__init__(self, self.title, self.skill)

    def __str__(self):
        exp_str = f'job titled: {self.name} , ' \
            + 'with skill: {self.skill} was not found'
        return exp_str


class JobByIdNotFound(Exception):
    def __init__(self, id):
        self.id = id
        super().__init__(self, self.id)

    def __str__(self):
        exp_str = f'job with id: {self.id} was not found'
        return exp_str


class JobAlreadyExists(Exception):
    def __init__(self, title, skill):
        self.title = title
        self.skill = skill
        super().__init__(self, self.title, self.skill)

    def __str__(self):
        exp_str = f'job titled: {self.title} ' \
            + 'with skill: {self.skill} already exists'
        return exp_str


class NoJobMatchesFound(Exception):
    def __init__(self, job_id):
        self.job_id = job_id
        super().__init__(self, self.job_id)
    def __str__(self):
        exp_str = 'no candidate matches found for job with id:' \
            + f' {self.job_id}'
        return exp_str


# candidate related exceptions 
class CandidateNotFound(Exception):
    def __init__(self, identifier):
        self.identifier = identifier
        super().__init__(self, self.identifier)

    def __str__(self):
        exp_str = 'Candidate with identifier:' \
            + f' {self.identifier} was not found'
        return exp_str


class CandidatesNotFound(Exception):
    def __init__(self, title, skill):
        self.title = title
        self.skill = skill
        super().__init__(self, self.title, self.skill)

    def __str__(self):
        exp_str = f'Candidates with title {self.title}' \
            + f' and skill {self.skill} were not found'
        return exp_str


class CandidateAlreadyExists(Exception):
    def __init__(self, name, skills):
        self.name = name
        super().__init__(self, self.name)

    def __str__(self):
        exp_str = 'Candidate named: ' \
            + f'{self.name} already exists'
        return exp_str
