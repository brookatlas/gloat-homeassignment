import json
from jsonschema import validate
from jsonschema import ValidationError
from django.http.response import JsonResponse
from django.http.request import HttpRequest
from functools import wraps


def singleton(class_):
    instances = {}
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance


def JsonSchemaValidated(schema:str):
    def schema_validated_decorator(func):
        @wraps(func)
        def schema_validate(view_class, request:HttpRequest, *args, **kwargs):
            try:
                body = json.loads(request.body.decode())
                validate(instance=body, schema=schema)
                return func(view_class, request, *args, **kwargs)
            except ValidationError as e:
                response = JsonResponse({
                    'schemaValidationError': e.message
                })
                response.status_code = 400
                return response
        return schema_validate
    return schema_validated_decorator


def NoEmptyBody(function):
    @wraps(function)
    def decorator(view_class, request: HttpRequest, *args, **kwargs):
        if request.body.decode() == '':
            response = JsonResponse({
                'error': 'body is empty'
            })
            response.status_code = 400
            return response
        else:
            return function(view_class, request, *args, **kwargs)
    return decorator
