import json
from candidatefinder.utils.decorators import JsonSchemaValidated, \
     NoEmptyBody
from candidatefinder.utils.exceptions import JobNotFound, JobAlreadyExists, \
     JobByIdNotFound
from candidatefinder.models.job import JobSchema
from candidatefinder.services.job import JobService
from django.http.request import HttpRequest
from django.http.response import JsonResponse
from django.views import View


class JobView(View):

    def get(self, request:HttpRequest):
        try:
            jobs = JobService().read()
            response = JsonResponse({
                'jobs': jobs
            })
            return response
        except JobNotFound as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 500
            return response

    @NoEmptyBody
    @JsonSchemaValidated(schema=JobSchema)
    def post(self, request:HttpRequest):
        try:
            body = json.loads(request.body.decode())
            job_created = JobService().create(body)
            response = JsonResponse(job_created)
            return response
        except JobAlreadyExists as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 409
            return response
        except Exception as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 500
            return response


class DetailedJobView(View):
    
    def get(self, request:HttpRequest, id:int):
        try:
            job = JobService().read_by_id(id)
            response = JsonResponse(job)
            return response
        except JobByIdNotFound as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 500
            return response

    def delete(self, request:HttpRequest, id:int):
        try:
            JobService().delete(id)
            return JsonResponse({
                'message': f'job with id {id} was deleted successfully'
            })
        except JobNotFound as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 500
            return response


class MatchJobView(View):
    def get(self, request:HttpRequest, id:int):
        try:
            matching_candidates = JobService().match(id)
            return JsonResponse({
                'matches': matching_candidates
            })
        except JobByIdNotFound as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 500
            return response
        
        return JsonResponse({
            "message": "this is the job match view"
        })
