import json
from django.views import View
from django.http import HttpRequest
from django.core.exceptions import ValidationError
from django.http.response import JsonResponse
from candidatefinder.services.candidate import CandidateService
from candidatefinder.models.candidate import CandidateSchema, \
     UpdateCandidateSchema
from candidatefinder.utils.exceptions import CandidateNotFound, \
     CandidateAlreadyExists
from candidatefinder.utils.decorators import NoEmptyBody, JsonSchemaValidated


class CandidateDetailView(View):
    def get(self, request:HttpRequest, id:int):
        try:
            result = CandidateService().read(id)
            return JsonResponse(result)
        except CandidateNotFound as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse({
                'error': 'unknown error'
            })
            response.status_code = 500
            return response
    
    @NoEmptyBody
    @JsonSchemaValidated(schema=UpdateCandidateSchema)
    def patch(self, request:HttpRequest, id:int):
        try:
            body = json.loads(request.body.decode())
            updated_candidate = CandidateService().update(id, body)
            response = JsonResponse(
                updated_candidate
            )
            return response
        except CandidateNotFound as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse({
                'error': 'unknown error'
            })
            response.status_code = 500
            return response

    def delete(self, request:HttpRequest, id:int):
        try:
            CandidateService().delete(id)
            return JsonResponse({
                'message': f'candidate with id {id} was deleted successfully'
            })
        except CandidateNotFound as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse({
                'error': 'unknown error'
            })
            response.status_code = 500
            return response


class CandidateView(View):
    
    def get(self, request:HttpRequest):
        try:
            candidates = CandidateService().read()
            response = JsonResponse({
                'candidates': candidates
            })
            return response
        except Exception as e:
            response = JsonResponse({
                'error': 'unknown error'
            })
            response.status_code = 500
            return response
    
    @NoEmptyBody
    @JsonSchemaValidated(schema=CandidateSchema)
    def post(self, request:HttpRequest):
        try:
            body = json.loads(request.body.decode())
            candidate_created = CandidateService().create(body)
            return JsonResponse(candidate_created)
        except CandidateAlreadyExists as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 409
            return response
        except ValidationError as e:
            response = JsonResponse({
                'error': e.__str__()
            })
            response.status_code = 400
            return response
        except Exception as e:
            response = JsonResponse({
                'error': 'unknown error'
            })
            response.status_code = 500
            return response
